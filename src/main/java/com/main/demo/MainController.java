package com.main.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/hlwld")
    public String getProperties(Model modelMap) {

        String string = "Hello, World!";

        modelMap.addAttribute("myAtribute", string);

        return "view";
    }
}
